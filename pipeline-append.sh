#!/bin/bash
## Cleanup bash script, in case pipeline failed and patch was applied 	##
# 	Only continues for languages that made it past the alignment step.
#	No overwrites.

## change 'en_fin_N/' below to point to your wiktToNicolai output	 	##
data=en_fin_N/

## change to the number of cores if running in parallel.				##
cores=1

#start local python environment
. ~/bin/activate

#set encoding to utf-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

#set permissions
perm=$(umask)
umask -p 0022

for lang in $(ls output/); do
	for File in $(ls output/"$lang"); do
		#find align file, continue pipeline from there
		if [[ "$File" == *.align ]]; then
			#split filename up using '_'
			str=(${File//_/ })
			#get pos index
			len=$[ ${#str[@]} - 1 ]
			#extract part of speech
			pos=${str[@]:$len:1}
			pos=${pos[0]}
			str=(${pos//./ })
			pos=${str[0]}

			#create model filename
			model=output/"$lang"/"${lang}"_"${pos}".model
			#find second-to-last model file
			mFile=$(find output/"$lang" -regex ""${model}".[0-9]+$" | \
				sort -t '.' -k 3,3n | tail -2 | head -1)
			if [ ! -s "$mFile" ]; then
				#train transducer
				printf "\n" | ./directlpCopy --order 1 --linearChain --cs 9 --ng 19 --jointMgram 5 \
				--copy  --inChar ':' -f output/"$lang"/"$File" --mo ${model}

				#find second-to-last model file
				mFile=$(find output/"$lang" -regex ""${model}".[0-9]+$" | \
					sort -t '.' -k 3,3n | tail -2 | head -1)
			fi

			#make testfile name
			test="$data""${lang}"_"${pos}"_test.txt
			#make output basename
			out=output/"$lang"/"${lang}"_"${pos}"

			if [ ! -s "$out" ]; then
				#test with model
				./directlpCopy --order 1 --linearChain --cs 9 --ng 19 --jointMgram 5 --copy \
				--inChar '|' --outChar ' ' -t "${test}" --nBestTest 10 -a "${out}" --mi "${mFile}"
			fi

			#make reranking filenames
			phrases="${out}".phraseOut
			paradigms="${out}"_paradigms.txt
			gold="$data""${lang}"_"${pos}"_gold.txt
			tags="$data""${lang}"_"${pos}"_tags.txt

			#java reranking
			if [ ! -s "${paradigms}" ]; then
				java -jar Reranker.jar extract "${phrases}" "${paradigms}" "${tags}"
			fi
			if [ ! -s "${out}"_LLtrain.txt ]; then
				java -jar Reranker.jar construct "${phrases}" "${paradigms}" "${tags}" \
				"${gold}" 10 False "${out}"_LLtrain.txt
			fi
			if [ ! -s "${out}"_LLtest.txt ]; then
				java -jar Reranker.jar construct "${phrases}" "${paradigms}" "${tags}" \
				"${gold}" 10 True "${out}"_LLtest.txt
			fi

			#liblinear reranking
			cp -p train llnrpredictr.py output/"$lang"/
			cd output/"$lang"/
			if [ ! -s "${lang}"_"${pos}"_LLtrain.txt.model ]; then
				if [[ "$cores" < 2 ]]; then
					./train "${lang}"_"${pos}"_LLtrain.txt && \
					./llnrpredictr.py "${lang}"_"${pos}"_LLtest.txt "${lang}"_"${pos}"_LLtrain.txt.model > \
					"${lang}"_"${pos}"_reranked.txt
				else
					./train -n "$cores" "${lang}"_"${pos}"_LLtrain.txt && \
					./llnrpredictr.py "${lang}"_"${pos}"_LLtest.txt "${lang}"_"${pos}"_LLtrain.txt.model > \
					"${lang}"_"${pos}"_reranked.txt
				fi
			elif [ ! -s "${lang}"_"${pos}"_reranked.txt ]; then
					./llnrpredictr.py "${lang}"_"${pos}"_LLtest.txt "${lang}"_"${pos}"_LLtrain.txt.model > \
					"${lang}"_"${pos}"_reranked.txt
			fi
			rm train llnrpredictr.py
			cd -
		fi
	done
done

#reset umask
umask -p "$perm"

#archive output
gunzip -q output.tar.gz
tar -uvf output.tar output/ || tar -cvf output.tar output/
gzip output.tar

exit